const verbose = true;

const utils = {
  config: {
    axios: require('./src/config/axios'),
    cors: require('./src/config/cors'),
    data: require('./src/config/data'),
    express: require('./src/config/express'),
    jwt: require('./src/config/jwt'),
    rule: require('./src/config/rule'),
  },
  controllers: {},
  enums: {},
  libs: {
    axios: require('axios'),
    bcrypt: require('bcrypt'),
    consign: require('consign'),
    cors: require('cors'),
    express: require('express'),
    fs: require('fs'),
    glob: require('glob'),
    jwt: require('jsonwebtoken'),
    middlewareChain: require('middleware-chain'),
    path: require('path'),
  },
  routes: {},
  services: {},
  utils: {},
};

// TODO: 09/07/2023 corrigir o consign, ta importando errado,
//  a estrutura do objeto utils ta ficando errada.
//  ele ta criando um parametro chamado projeto dentro do utils
utils.libs
  .consign({ cwd: `${__dirname}/src`, extensions: ['.js'], verbose })
  .include('enums')
  .include('config')
  .into(utils);

utils.logger = console;

const utilsFiles = utils.libs.glob.globSync(utils.libs.path.join(__dirname, 'src', 'utils', `!(*.test).js`));
const servicesFiles = utils.libs.glob.sync(`${__dirname}/src/services/!(*.test).js`);
const controllersFiles = utils.libs.glob.sync(`${__dirname}/src/controllers/!(*.test).js`);
const routesFiles = utils.libs.glob.sync(`${__dirname}/src/routes/!(*.test).js`);

let consignInstance = utils.libs.consign({ cwd: `src`, verbose });
[...utilsFiles, ...servicesFiles, ...controllersFiles, ...routesFiles].forEach(file => {
  consignInstance = consignInstance.include(utils.libs.path.relative('src', file));
});

consignInstance.into(utils);

utils.jwtSecret = utils.config.jwt.jwtSecret;
utils.axios = utils.libs.axios.create(utils.config.axios);

module.exports = utils;
