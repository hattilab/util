const ClientRoutes = app => {
  const router = app.libs.express.Router();
  const controller = app.controllers.ClientController;

  // Rota para obter todos os clientes
  router.get('/', controller.getClients);

  // Rota para obter um cliente pelo ID
  router.get('/:id', controller.getClientById);

  // Rota para obter um cliente pelo nome
  router.get('/name/:name', controller.getClientByName);

  // Rota para criar um novo cliente
  router.post('/', controller.createClient);

  // Rota para atualizar um cliente pelo ID
  router.put('/:id', controller.updateClient);

  // Rota para excluir um cliente pelo ID
  router.delete('/:id', controller.deleteClient);

  return router;
};

module.exports = ClientRoutes;
