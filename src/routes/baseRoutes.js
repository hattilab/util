const BaseRoutes = app => {
  const newRoutes = routes => {
    if (!Array.isArray(routes)) {
      throw new Error('Routes must be an array');
    }

    if (routes.length === 0) {
      throw new Error('Routes array is empty');
    }

    const supportedMethods = app?.config?.express?.supportedMethods;

    if (!supportedMethods) {
      throw new Error('Supported HTTP methods not configured');
    }

    const router = app?.libs?.express?.Router();

    if (!router) {
      throw new Error('Express router not available');
    }

    for (const route of routes) {
      const { method, path, action } = route;

      if (!method) {
        throw new Error('Route object is missing method property');
      }
      if (!path) {
        throw new Error('Route object is missing path property');
      }
      if (!action) {
        throw new Error('Route object is missing action property');
      }

      if (!supportedMethods.includes(method)) {
        throw new Error(`Unsupported HTTP method: ${method}`);
      }

      router[method](path, action);
    }

    return router;
  };

  return {
    newRoutes,
  };
};

module.exports = BaseRoutes;
