const RequestRoutes = app => {
  const router = app.libs.express.Router();
  const { controllers } = app;
  const { RequestController } = controllers;

  // Rota para receber requisições POST
  router.all('/', RequestController.handleRequest);

  return router;
};

module.exports = RequestRoutes;
