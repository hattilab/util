const BaseRoutes = require('./baseRoutes');

describe('BaseRoutes', () => {
  let appMock;
  let routerMock;
  let baseRoutes;

  beforeEach(() => {
    routerMock = {
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
      delete: jest.fn(),
    };

    appMock = {
      libs: {
        express: {
          Router: jest.fn().mockReturnValue(routerMock),
        },
      },
      config: {
        express: {
          supportedMethods: ['get', 'post', 'put', 'delete'],
        },
      },
    };

    baseRoutes = BaseRoutes(appMock);
  });

  describe('newRoutes', () => {
    test('should return an object with newRoutes method', () => {
      expect(baseRoutes).toHaveProperty('newRoutes');
      expect(typeof baseRoutes.newRoutes).toBe('function');
    });

    test('should create a new router instance', () => {
      const routes = [
        {
          method: 'get',
          path: '/',
          action: jest.fn(),
        },
      ];
      const router = baseRoutes.newRoutes(routes);

      expect(appMock.libs.express.Router).toHaveBeenCalled();
      expect(router).toBe(routerMock);
    });

    test('should define correct routes on the router', () => {
      const routes = [
        {
          method: 'get',
          path: '/',
          action: jest.fn(),
        },
        {
          method: 'post',
          path: '/users',
          action: jest.fn(),
        },
      ];
      baseRoutes.newRoutes(routes);

      expect(routerMock.get).toHaveBeenCalledWith('/', expect.any(Function));
      expect(routerMock.post).toHaveBeenCalledWith('/users', expect.any(Function));
    });

    test('should handle different HTTP methods', () => {
      const routes = [
        {
          method: 'get',
          path: '/',
          action: jest.fn(),
        },
        {
          method: 'post',
          path: '/users',
          action: jest.fn(),
        },
        {
          method: 'put',
          path: '/users/:id',
          action: jest.fn(),
        },
        {
          method: 'delete',
          path: '/users/:id',
          action: jest.fn(),
        },
      ];
      baseRoutes.newRoutes(routes);

      expect(routerMock.get).toHaveBeenCalledWith('/', expect.any(Function));
      expect(routerMock.post).toHaveBeenCalledWith('/users', expect.any(Function));
      expect(routerMock.put).toHaveBeenCalledWith('/users/:id', expect.any(Function));
      expect(routerMock.delete).toHaveBeenCalledWith('/users/:id', expect.any(Function));
    });

    test('should handle dynamic paths', () => {
      const routes = [
        {
          method: 'get',
          path: '/users/:id',
          action: jest.fn(),
        },
      ];
      baseRoutes.newRoutes(routes);

      expect(routerMock.get).toHaveBeenCalledWith('/users/:id', expect.any(Function));
    });

    test('should handle routes with optional parameters', () => {
      const routes = [
        {
          method: 'get',
          path: '/users/:id?',
          action: jest.fn(),
        },
      ];
      const router = baseRoutes.newRoutes(routes);

      expect(routerMock.get).toHaveBeenCalledWith('/users/:id?', expect.any(Function));
    });

    test('should throw an error if routes is not an array', () => {
      expect(() => {
        baseRoutes.newRoutes();
      }).toThrow('Routes must be an array');
    });

    test('should throw an error if routes array is empty', () => {
      const routes = []; // Array vazio
      expect(() => {
        baseRoutes.newRoutes(routes);
      }).toThrow('Routes array is empty');
    });

    test('should throw an error if route object is missing method property', () => {
      const routes = [
        {
          path: '/',
          action: jest.fn(),
        },
      ];
      expect(() => {
        baseRoutes.newRoutes(routes);
      }).toThrow('Route object is missing method property');
    });

    test('should throw an error if route object is missing path property', () => {
      const routes = [
        {
          method: 'get',
          action: jest.fn(),
        },
      ];
      expect(() => {
        baseRoutes.newRoutes(routes);
      }).toThrow('Route object is missing path property');
    });

    test('should throw an error if route object is missing action property', () => {
      const routes = [
        {
          method: 'get',
          path: '/',
        },
      ];
      expect(() => {
        baseRoutes.newRoutes(routes);
      }).toThrow('Route object is missing action property');
    });

    test('should throw an error if route method is not supported', () => {
      const routes = [
        {
          method: 'invalid',
          path: '/',
          action: jest.fn(),
        },
      ];
      expect(() => {
        baseRoutes.newRoutes(routes);
      }).toThrow('Unsupported HTTP method: invalid');
    });
  });
});
