const ProductRoutes = app => {
  const router = app.libs.express.Router();
  const controller = app.controllers.ProductController;

  // Rota para obter todos os produtos
  router.get('/', controller.getProducts);

  // Rota para obter um produto pelo ID
  router.get('/:id', controller.getProductById);

  // Rota para criar um novo produto
  router.post('/', controller.createProduct);

  // Rota para atualizar um produto pelo ID
  router.put('/:id', controller.updateProduct);

  // Rota para excluir um produto pelo ID
  router.delete('/:id', controller.deleteProduct);
  return router;
};
module.exports = ProductRoutes;
