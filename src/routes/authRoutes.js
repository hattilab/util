const AuthRoutes = app => {
  const router = app.libs.express.Router();
  const controller = app.controllers.AuthController;

  // Rota para registro de novo usuário
  router.post('/register', controller.register);

  // Rota para login de usuário
  router.post('/login', controller.login);

  // Rota para logout de usuário
  router.post('/logout', controller.logout);
  return router;
};

module.exports = AuthRoutes;
