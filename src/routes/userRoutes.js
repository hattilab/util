function UserRoutes(app) {
  const router = app.libs.express.Router();
  const controller = app.controllers.UserController;

  // Rota para obter todos os usuários
  router.get('/', controller.getUsers);

  // Rota para obter um usuário pelo ID
  router.get('/:id', controller.getUserById);

  // Rota para obter um usuário pelo email
  router.get('/email/:email', controller.getUserByEmail);

  // Rota para criar um novo usuário
  router.post('/', controller.createUser);

  // Rota para atualizar um usuário pelo ID
  router.put('/:id', controller.updateUser);

  // Rota para excluir um usuário pelo ID
  router.delete('/:id', controller.deleteUser);
  return router;
}

module.exports = UserRoutes;
