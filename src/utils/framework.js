const Framework = app => {
  const createFunction = ({ fn, validadetion, results }) => {
    return (...args) => {
      try {
        const validationResult = validadetion(args);

        if (!validationResult.status) {
          return results.failure(validationResult);
        }

        const result = fn(args);

        if (!result) {
          return results.failure(result);
        }

        return results.success(result);
      } catch (error) {
        // TODO: 20/06/2023 mellhorar o fallback.
        // app.logger.log(error);
        if (typeof results?.error === 'function') {
          return results?.error(error);
        }
        return error;
      }
    };
  };

  const createFunction2 = ({ fn, validadetion, results }) => {
    return (...args) => {
      try {
        const validationResult = validadetion(args);

        if (validationResult) {
          return results(validationResult);
        }

        return results(fn(args));
      } catch (error) {
        // TODO: 20/06/2023 mellhorar o fallback.
        app.logger.error(error);
        return results(error);
      }
    };
  };

  const middlewareChain = (middlewares, chain, context) => chain(context, middlewares);

  const createMiddleware = middleware => {
    return middleware;
  };

  const createServer = ({ appName, server, middlewares }) => {
    middlewares.forEach(middleware => {
      server.use(middleware);
    });

    server.appName = appName;

    return server;
  };
  const startServer = ({ app, config, logger }) => {
    const port = config[app.appName]?.port || process?.env?.PORT || 3003; // TODO: 20/06/2023 melhora o fallback
    app.listen(port, () => {
      logger.log(`Server listening at http://localhost:${port}`);
    });
  };

  return {
    createFunction,
    createFunction2,
    middlewareChain,
    createMiddleware,
    createServer,
    startServer,
  };
};

module.exports = Framework;
