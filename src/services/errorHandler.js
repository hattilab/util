const ErrorHandler = app => {
  function handleError(error, errorHandler) {
    if (typeof errorHandler === 'function') {
      // Se um manipulador de erros personalizado for fornecido, chame-o
      errorHandler(error);
    } else {
      // Caso contrário, faça um tratamento padrão
      console.error('Ocorreu um erro:', error);
      // Outras ações podem ser executadas aqui, como enviar notificações, gravar logs, etc.
    }
  }

  return {
    handleError,
  };
};

module.exports = ErrorHandler;
