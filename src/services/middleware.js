module.exports = app => {
  this.basicMiddleware = basicMiddleware;

  function basicMiddleware(fn) {
    return (context, next) => {
      fn(context);
      return next();
    };
  }

  return this;
};
