const Validator = app => {
  const createValidator = asd => {
    const validator = app.validator.createValidator(asd);

    return validator;
  };

  const createValidators = validatorsList => {
    const results = {};

    validatorsList.forEach(result => {
      results[result.label] = app.validator.createResult(validatorsList);
    });

    return results;
  };
  //
  // const validate = (data, schema) => {
  //   const { error, value } = schema.validate(data);
  //
  //   if (error) {
  //     throw new Error(error.details[0].message);
  //   }
  //
  //   return value;
  // };
  //
  // const validadeParam = params => {
  //   try {
  //     const result = validate(data, schema);
  //
  //     if (!result) {
  //       return app.results.failure(result);
  //     }
  //
  //     return app.results.success(result);
  //   } catch (error) {
  //     return app.utils.results.error(error);
  //   }
  // };

  return {
    createValidator,
    createValidators,
    // validadeParam,
    // validate,
  };
};

module.exports = Validator;
