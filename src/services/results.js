const Results = app => {
  const createResult = fn => {
    return (...args) => {
      try {
        return fn(args);
      } catch (error) {
        // TODO: 20/06/2023 mellhorar o fallback.
        app.logger.error(error);
        if (typeof results?.error === 'function') {
          return results?.error(error);
        }
        return error;
      }
    };
  };

  const createResults = resultList => {
    const results = {};

    resultList.forEach(result => {
      results[result.label] = app.results.createResult(result);
    });

    return results;
  };
  //
  // const success = data => {
  //   return {
  //     status: true,
  //     data,
  //   };
  // };
  //
  // const failure = message => {
  //   return {
  //     status: false,
  //     message,
  //   };
  // };
  //
  // const error = error => {
  //   let newError = error;
  //
  //   if (typeof error === 'string') {
  //     newError = { message: error };
  //   }
  //
  //   return {
  //     status: false,
  //     error: newError,
  //   };
  // };

  return {
    createResult,
    createResults,
    // success,
    // failure,
    // error,
  };
};

module.exports = Results;
