module.exports = app => {
  this.create = ({ libs }) => {
    return libs.express();
  };

  this.start = ({ server, middlewares }) => {
    // Configurações do Express
    // TODO: 20/06/2023 melhorar. criar uma função pra encapsular toda a logica.
    // server.use(middlewares.express.json()); // Habilitar o uso de JSON no corpo das requisições

    // Configuração do CORS
    // TODO: 20/06/2023 melhorar. criar uma função pra encapsular toda a logica.
    // server.use(middlewares.cors(config.cors));
  };

  return this;
};
