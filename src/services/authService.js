const AuthService = app => {
  const { jwt, jwtSecret, logger } = app;
  const generateToken = user => {
    try {
      const payload = {
        id: user.id,
        email: user.email,
      };

      return jwt.sign(payload, jwtSecret, { expiresIn: '1h' });
    } catch (error) {
      logger.error(error);
      throw new Error('Failed to generate token');
    }
  };

  return {
    generateToken,
  };
};

module.exports = AuthService;
