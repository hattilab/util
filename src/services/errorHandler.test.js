const ErrorHandler = require('./errorHandler');

describe('ErrorHandler', () => {
  let app;
  let errorHandler;

  beforeEach(() => {
    app = {};
    errorHandler = ErrorHandler(app);
  });

  describe('handleError', () => {
    afterEach(() => {
      jest.restoreAllMocks(); // Restaurar todas as mocks do Jest
    });

    it('should call the custom error handler if provided', () => {
      const customHandler = jest.fn();
      const error = new Error('Custom error');

      errorHandler.handleError(error, customHandler);

      expect(customHandler).toHaveBeenCalledWith(error);
    });

    it('should log the error if no custom error handler is provided', () => {
      const consoleSpy = jest.spyOn(console, 'error').mockImplementation();

      const error = new Error('Standard error');

      errorHandler.handleError(error);

      expect(console.error).toHaveBeenCalledWith('Ocorreu um erro:', error);

      consoleSpy.mockRestore();
    });

    it('should allow handling different error types', () => {
      const customHandler = jest.fn();

      const error1 = new Error('Error 1');
      const error2 = { message: 'Error 2' };

      errorHandler.handleError(error1, customHandler);
      errorHandler.handleError(error2, customHandler);

      expect(customHandler).toHaveBeenCalledTimes(2);
      expect(customHandler).toHaveBeenCalledWith(error1);
      expect(customHandler).toHaveBeenCalledWith(error2);
    });

    it('should not throw an error if no error handler is provided', () => {
      const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

      const error = new Error('Standard error');
      expect(() => {
        errorHandler.handleError(error);
      }).not.toThrow();

      expect(consoleErrorSpy).toHaveBeenCalledWith('Ocorreu um erro:', error);
    });

    it('should log the error using console.error', () => {
      const consoleSpy = jest.spyOn(console, 'error').mockImplementation();

      const error = new Error('Standard error');

      errorHandler.handleError(error);

      expect(console.error).toHaveBeenCalledWith('Ocorreu um erro:', error);

      consoleSpy.mockRestore();
    });

    it('should handle a custom error object', () => {
      const customHandler = jest.fn();

      const error = { message: 'Custom error', code: 500 };

      errorHandler.handleError(error, customHandler);

      expect(customHandler).toHaveBeenCalledWith(error);
    });

    it('should handle an error thrown by the custom error handler', () => {
      const customHandler = jest.fn(() => {
        throw new Error('Error within custom error handler');
      });

      const error = new Error('Standard error');

      expect(() => {
        errorHandler.handleError(error, customHandler);
      }).toThrow('Error within custom error handler');

      expect(customHandler).toHaveBeenCalledWith(error);
    });

    it('should call the custom error handler if provided', () => {
      const customErrorHandler = jest.fn();

      const error = new Error('Custom error');
      errorHandler.handleError(error, customErrorHandler);

      expect(customErrorHandler).toHaveBeenCalledWith(error);
    });
  });
});
