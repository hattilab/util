// userService.js
const UserService = app => {
  const { logger, axios, bcrypt } = app;

  const getUserByEmail = async email => {
    try {
      const response = await axios.get(`/users/email/${email}`);
      const users = response.data;
      if (users.length > 0) {
        return users[0];
      }
      return null;
    } catch (error) {
      logger.error(error);
      throw new Error('Failed to get user by email');
    }
  };

  const comparePassword = async (password, hashedPassword) => {
    try {
      if (!password || !hashedPassword) {
        return false;
      }
      return await bcrypt.compare(password, hashedPassword);
    } catch (error) {
      logger.error(error);
      throw new Error('Failed to compare passwords');
    }
  };

  const createUser = async user => {
    try {
      const response = await axios.post('/users', user);
      return response.data;
    } catch (error) {
      logger.error(error);
      throw new Error('Failed to create user');
    }
  };

  return {
    getUserByEmail,
    comparePassword,
    createUser,
  };
};

module.exports = UserService;
