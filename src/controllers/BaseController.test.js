const BaseController = require('./BaseController');
const ErrorHandler = require('../utils/errorHandler');

describe('BaseController', () => {
  let app;
  let controller;

  beforeEach(() => {
    app = {
      mockUsers: [
        { id: 1, name: 'John Doe', email: 'john@example.com', password: '123456' },
        { id: 2, name: 'Jane Smith', email: 'jane@example.com', password: 'abcdef' },
      ],
      errorHandler: ErrorHandler(app),
      logger: {
        error: jest.fn(), // Definindo a função error no logger como um mock
      },
    };
    controller = BaseController(app);
  });

  describe('handleRequest', () => {
    const mockParams = { id: 1, name: 'John Doe' };

    it('should execute the action and return the result if validation passes', () => {
      const mockValidate = jest.fn(() => ({ error: null }));
      const mockAction = jest.fn(() => 'Result');

      const result = controller.handleRequest(mockParams, mockValidate, mockAction);

      expect(mockValidate).toHaveBeenCalledWith(mockParams);
      expect(mockAction).toHaveBeenCalledWith(mockParams);
      expect(result).toBe('Result');
    });

    it('should throw an error if validation fails', () => {
      const mockValidate = jest.fn(() => ({ error: 'Validation error' }));
      const mockAction = jest.fn(() => 'Result');

      expect(() => {
        controller.handleRequest(mockParams, mockValidate, mockAction);
      }).toThrow('Erro ao processar a requisição: Validation error');

      expect(mockValidate).toHaveBeenCalledWith(mockParams);
      expect(mockAction).not.toHaveBeenCalled();
    });

    it('should throw an error if an error occurs during action execution', () => {
      // Mock da função de ação que lança um erro
      const action = jest.fn(() => {
        throw new Error('Action error');
      });

      // Mock da função de validação que retorna um objeto de erro
      const validate = jest.fn(() => {
        return 'Validation error';
      });

      // Chama a função handleRequest passando a ação, a validação e a mensagem de erro personalizada
      const errorMessage = 'Erro ao executar o método getList';
      expect(() => {
        controller.handleRequest({}, validate, action, errorMessage);
      }).toThrow(`Erro ao executar o método getList: Action error`);
    });

    it('should handle and throw a custom error message if provided', () => {
      const mockValidate = jest.fn(() => ({ error: 'Validation error' }));
      const mockAction = jest.fn(() => 'Result');
      const errorMessage = 'Custom error message';

      expect(() => {
        controller.handleRequest(mockParams, mockValidate, mockAction, errorMessage);
      }).toThrow(`Custom error message: Validation error`);
    });

    it('should handle and throw a default error message if no custom message is provided', () => {
      const mockValidate = jest.fn(() => ({ error: 'Validation error' }));
      const mockAction = jest.fn(() => 'Result');

      expect(() => {
        controller.handleRequest(mockParams, mockValidate, mockAction);
      }).toThrow('Erro ao processar a requisição');
    });

    it('should handle validation errors correctly', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: 'Invalid parameters' }));
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Invalid parameters');
    });

    it('should handle action errors correctly', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Action failed');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Action failed');
    });

    it('should throw an error when the validation function is not provided', () => {
      const params = {}; // Parâmetros de teste
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, undefined, action);
      }).toThrow('Erro ao processar a requisição: Erro de validação: função de validação não fornecida ou inválida');
    });

    it('should throw an error when no parameters are provided', () => {
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(undefined, validate, action);
      }).toThrow('Erro ao processar a requisição: No parameters provided');
    });

    it('should throw an error when the validation function returns an unknown error', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => {
        throw new Error('Unknown validation error');
      });
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Unknown validation error');
    });

    it('should throw an error when the action function is not provided', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));

      expect(() => {
        controller.handleRequest(params, validate, undefined);
      }).toThrow('Erro ao processar a requisição: Action function is not provided');
    });
  });

  describe('createController', () => {
    const mockRules = [
      {
        name: 'getList',
        validate: jest.fn(() => ({ error: null })),
        action: jest.fn(),
      },
      {
        name: 'getOneById',
        validate: jest.fn(() => ({ error: null })),
        action: jest.fn(),
      },
    ];

    it('should create a controller object with methods based on the rules', () => {
      const newController = controller.createController(mockRules);

      expect(newController).toBeDefined();
      expect(typeof newController).toBe('object');

      expect(newController.getList).toBeDefined();
      expect(typeof newController.getList).toBe('function');

      // Simulate calling the getList method
      const req = {};
      const res = { json: jest.fn() };
      newController.getList(req, res);
      expect(mockRules[0].validate).toHaveBeenCalled();
      expect(mockRules[0].action).toHaveBeenCalled();

      expect(newController.getOneById).toBeDefined();
      expect(typeof newController.getOneById).toBe('function');

      // Simulate calling the getOneById method
      const req2 = { params: { id: 1 } };
      const res2 = { json: jest.fn(), status: jest.fn().mockReturnThis() };
      newController.getOneById(req2, res2);
      expect(mockRules[1].validate).toHaveBeenCalled();
      expect(mockRules[1].action).toHaveBeenCalled();
    });

    it('should not create methods for rules that are not defined', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(),
          action: jest.fn(),
        },
        // ... adicione outras regras válidas
      ];

      const newController = controller.createController(rules);

      expect(newController.getList).toBeDefined();
      expect(newController.getOneById).toBeUndefined();
      // ... adicione verificações para outros métodos não definidos
    });

    it('should create an empty controller when no rules are defined', () => {
      const rules = [];

      const newController = controller.createController(rules);

      expect(newController).toEqual({});
    });

    it('should call the appropriate validation and action functions for each method', () => {
      const newController = controller.createController(mockRules);

      const req = {};
      const res = { json: jest.fn() };

      newController.getList(req, res);
      expect(mockRules[0].validate).toHaveBeenCalled();
      expect(mockRules[0].action).toHaveBeenCalled();

      newController.getOneById(req, res);
      expect(mockRules[1].validate).toHaveBeenCalled();
      expect(mockRules[1].action).toHaveBeenCalled();

      // ... adicione mais verificações para outros métodos, se houver
    });

    it('should throw an error if an error occurs during action execution', () => {
      const mockError = new Error('Action error');
      mockRules[0].action.mockImplementation(() => {
        throw mockError;
      });

      expect(() => {
        const newController = controller.createController(mockRules);
        const req = {};
        const res = { json: jest.fn() };
        newController.getList(req, res);
      }).toThrow(`Erro ao processar a requisição: ${mockError.message}`);

      expect(app.logger.error).toHaveBeenCalledWith(expect.stringContaining('Erro ao executar o método getList'));
    });

    it('should contain only the methods specified in the rules', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(),
          action: jest.fn(),
        },
        {
          name: 'getOneById',
          validate: jest.fn(),
          action: jest.fn(),
        },
      ];

      const newController = controller.createController(rules);

      const expectedMethods = ['getList', 'getOneById'];
      const controllerMethods = Object.keys(newController);

      expect(controllerMethods).toEqual(expectedMethods);
    });

    it('should return the expected results when methods are called', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(() => {
            return 'validate result';
          }),
          action: jest.fn(() => ['item1', 'item2']),
        },
        {
          name: 'getOneById',
          validate: jest.fn(() => {
            return 'validate result';
          }),
          action: jest.fn(id => ({ id, name: 'John Doe' })),
        },
      ];

      const newController = controller.createController(rules);

      const getListResult = newController.getList();
      expect(getListResult).toEqual(['item1', 'item2']);

      const getOneByIdResult = newController.getOneById(123);
      expect(getOneByIdResult).toEqual({ id: 123, name: 'John Doe' });
    });

    it('should call the methods with the correct parameters', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(),
          action: jest.fn(),
        },
        {
          name: 'getOneById',
          validate: jest.fn(),
          action: jest.fn(),
        },
      ];

      const newController = controller.createController(rules);

      const getListParams = { page: 1, limit: 10 };
      newController.getList(getListParams);
      expect(rules[0].validate).toHaveBeenCalledWith(getListParams);
      expect(rules[0].action).toHaveBeenCalledWith(getListParams);

      const getOneByIdParams = { id: 123 };
      newController.getOneById(getOneByIdParams);
      expect(rules[1].validate).toHaveBeenCalledWith(getOneByIdParams);
      expect(rules[1].action).toHaveBeenCalledWith(getOneByIdParams);
    });

    it('should create separate methods for different rules in the controller object', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(),
          action: jest.fn(),
        },
        {
          name: 'getOneById',
          validate: jest.fn(),
          action: jest.fn(),
        },
      ];

      const newController = controller.createController(rules);

      expect(newController.getList).not.toBe(newController.getOneById);
    });

    it('should call the validate function before executing the action', () => {
      const validateFn = jest.fn(() => ({ error: null }));
      const actionFn = jest.fn();
      const rule = {
        name: 'getList',
        validate: validateFn,
        action: actionFn,
      };
      const rules = [rule];

      const newController = controller.createController(rules);
      const req = {};
      const res = { json: jest.fn() };

      newController.getList(req, res);

      expect(validateFn).toHaveBeenCalledWith(req);
      expect(actionFn).toHaveBeenCalled();
    });

    it('should call the action function with the correct parameters', () => {
      const validateFn = jest.fn(() => ({ error: null }));
      const actionFn = jest.fn();
      const rule = {
        name: 'getList',
        validate: validateFn,
        action: actionFn,
      };
      const rules = [rule];

      const newController = controller.createController(rules);
      const req = { params: { id: 1 }, query: { sort: 'asc' } };
      const res = { json: jest.fn() };

      newController.getList(req, res);

      expect(actionFn).toHaveBeenCalledWith({ params: { id: 1 }, query: { sort: 'asc' } });
    });

    it('should call the validate function when executing a controller method', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(() => ({ error: null })),
          action: jest.fn(),
        },
      ];

      const newController = controller.createController(rules);
      const req = {};
      const res = { json: jest.fn() };

      newController.getList(req, res);

      expect(rules[0].validate).toHaveBeenCalledWith(req);
    });

    it('should call the action function when executing a controller method', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(() => ({ error: null })),
          action: jest.fn(),
        },
      ];

      const newController = controller.createController(rules);
      const req = {};
      const res = { json: jest.fn() };

      newController.getList(req, res);

      expect(rules[0].action).toHaveBeenCalledWith(req);
    });

    it('should throw an error when validation fails', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: 'Invalid parameters' }));
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Invalid parameters');
    });

    it('should throw an error when an error occurs during action execution', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Action error');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Action error');
    });

    it('should throw an error when a rule does not have a name', () => {
      const rules = [
        {
          validate: jest.fn(),
          action: jest.fn(),
        },
      ];

      expect(() => {
        controller.createController(rules);
      }).toThrow('A rule must have a name');
    });

    it('should throw an error when a rule does not have a validation function', () => {
      const rules = [
        {
          name: 'getList',
          action: jest.fn(),
        },
      ];

      expect(() => {
        controller.createController(rules);
      }).toThrow('A rule must have a validation function');
    });

    it('should throw an error when a rule does not have an action function', () => {
      const rules = [
        {
          name: 'getList',
          validate: jest.fn(),
        },
      ];

      expect(() => {
        controller.createController(rules);
      }).toThrow('A rule must have an action function');
    });

    it('should throw an error when an unknown error occurs during action execution', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Unknown error');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Unknown error');
    });

    it('should throw an error when the validation function returns an error', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: 'Validation error' }));
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Validation error');
    });

    it('should throw an error when an error occurs during action execution with custom error message', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Action error');
      });
      const errorMessage = 'Erro ao executar a ação';

      expect(() => {
        controller.handleRequest(params, validate, action, errorMessage);
      }).toThrow('Erro ao executar a ação: Action error');
    });

    it('should throw an error with default error message when an unknown error occurs during action execution', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Unknown error');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Unknown error');
    });

    it('should throw an error when an error occurs during action execution with no error message', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Action error');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Action error');
    });

    it('should throw a custom error when validation returns an error with a specific message', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: 'Invalid parameters' }));
      const action = jest.fn();

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Invalid parameters');
    });

    it('should throw a custom error when action returns an error with a specific message', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Action failed: Invalid input');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Action failed: Invalid input');
    });

    it('should throw a generic error message when an unknown error occurs in the action', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: null }));
      const action = jest.fn(() => {
        throw new Error('Unknown error');
      });

      expect(() => {
        controller.handleRequest(params, validate, action);
      }).toThrow('Erro ao processar a requisição: Ocorreu um erro inesperado');
    });

    it('should throw a custom error when validation returns an error and custom error message is provided', () => {
      const params = {}; // Parâmetros de teste
      const validate = jest.fn(() => ({ error: 'Invalid parameters' }));
      const action = jest.fn();
      const errorMessage = 'Erro ao processar a requisição: Parâmetros inválidos';

      expect(() => {
        controller.handleRequest(params, validate, action, errorMessage);
      }).toThrow('Erro ao processar a requisição: Parâmetros inválidos');
    });
  });

  describe('getList', () => {
    it('should return the list of users', () => {
      const req = {};
      const res = {
        json: jest.fn(),
      };

      controller.getList(req, res);

      expect(res.json).toHaveBeenCalledWith(app.mockUsers);
    });
  });

  // describe('getOneById', () => {
  //   it('should return the user with the matching ID if found', () => {
  //     const req = {
  //       params: { id: '1' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //       status: jest.fn().mockReturnThis(),
  //     };
  //
  //     controller.getOneById(req, res);
  //
  //     expect(res.json).toHaveBeenCalledWith(app.mockUsers[0]);
  //   });
  //
  //   it('should return 404 error if the user with the matching ID is not found', () => {
  //     const req = {
  //       params: { id: '999' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //       status: jest.fn().mockReturnThis(),
  //     };
  //
  //     controller.getOneById(req, res);
  //
  //     expect(res.status).toHaveBeenCalledWith(404);
  //     expect(res.json).toHaveBeenCalledWith({ error: 'Usuário não encontrado' });
  //   });
  // });
  //
  // describe('getOneByField', () => {
  //   it('should return the user with the matching email if found', () => {
  //     const req = {
  //       params: { email: 'john@example.com' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //       status: jest.fn().mockReturnThis(),
  //     };
  //
  //     controller.getOneByField(req, res);
  //
  //     expect(res.json).toHaveBeenCalledWith(app.mockUsers[0]);
  //   });
  //
  //   it('should return 404 error if the user with the matching email is not found', () => {
  //     const req = {
  //       params: { email: 'unknown@example.com' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //       status: jest.fn().mockReturnThis(),
  //     };
  //
  //     controller.getOneByField(req, res);
  //
  //     expect(res.status).toHaveBeenCalledWith(404);
  //     expect(res.json).toHaveBeenCalledWith({ error: 'Usuário não encontrado' });
  //   });
  // });
  //
  // describe('createOne', () => {
  //   it('should create a new user', () => {
  //     const req = {};
  //     const res = {
  //       status: jest.fn().mockReturnThis(),
  //       json: jest.fn(),
  //     };
  //
  //     controller.createOne(req, res);
  //
  //     expect(res.status).toHaveBeenCalledWith(201);
  //     expect(res.json).toHaveBeenCalledWith({ success: true });
  //   });
  // });
  //
  // describe('updateById', () => {
  //   it('should update the user with the matching ID', () => {
  //     const req = {
  //       params: { id: '1' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //     };
  //
  //     controller.updateById(req, res);
  //
  //     // Add your assertions for the update logic here
  //   });
  // });
  //
  // describe('deleteById', () => {
  //   it('should delete the user with the matching ID', () => {
  //     const req = {
  //       params: { id: '1' },
  //     };
  //     const res = {
  //       json: jest.fn(),
  //     };
  //
  //     controller.deleteById(req, res);
  //
  //     // Add your assertions for the delete logic here
  //   });
  // });
});
