const AuthController = app => {
  const { logger, services } = app;
  const { userService, authService } = services;

  const register = async (req, res) => {
    try {
      // Obtenha os dados do corpo da requisição
      const { name, email, password } = req.body;

      // Verifique se o usuário já está cadastrado
      const existingUser = await userService.getUserByEmail(email);
      if (existingUser) {
        return res.status(400).json({ message: 'Email already registered' });
      }

      // Crie um usuário
      const newUser = await userService.createUser({ name, email, password });

      // Gere um ‘token’ de autenticação
      const token = authService.generateToken(newUser);

      // Retorne os dados do usuário e o ‘token’
      return res.json({ user: newUser, token });
    } catch (error) {
      logger.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
  };

  const login = async (req, res) => {
    try {
      // Obtenha os dados do corpo da requisição
      const { email, password } = req.body;

      // Verifique se o usuário existe
      const user = await userService.getUserByEmail(email);
      if (!user) {
        return res.status(401).json({ message: 'Invalid email or password' });
      }

      // Verifique se a senha está correta
      const isPasswordValid = await userService.comparePassword(password, user.password);
      if (!isPasswordValid) {
        return res.status(401).json({ message: 'Invalid email or password' });
      }

      // Gere um ‘token’ de autenticação
      const token = authService.generateToken(user);

      // Retorne os dados do usuário e o ‘token’
      return res.json({ user, token });
    } catch (error) {
      logger.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
  };

  const logout = async (req, res) => {
    try {
      // Implemente a lógica para fazer logout, como invalidar o token, remover cookies, etc.

      // Retorne uma resposta de sucesso
      return res.json({ message: 'Logged out successfully' });
    } catch (error) {
      logger.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
  };

  return {
    register,
    login,
    logout,
  };
};

module.exports = AuthController;
