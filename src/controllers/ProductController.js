// ProductController.js

const mockProducts = [
  { id: 1, name: 'Product 1', price: 10.99 },
  { id: 2, name: 'Product 2', price: 19.99 },
  // Outros produtos mockados...
];

const getProducts = (req, res) => {
  res.json(mockProducts);
};

const getProductById = (req, res) => {
  const { id } = req.params;
  const product = mockProducts.find(product => product.id === parseInt(id));
  if (product) {
    res.json(product);
  } else {
    res.status(404).json({ error: 'Produto não encontrado' });
  }
};

const createProduct = (req, res) => {
  // Lógica para criar um novo produto com base nos dados da requisição
  // e adicionar o produto à lista de produtos mockados
  // ...
  res.status(201).json({ success: true });
};

const updateProduct = (req, res) => {
  const { id } = req.params;
  // Lógica para atualizar os dados do produto com base nos dados da requisição
  // e atualizar o produto na lista de produtos mockados
  // ...
  res.json({ success: true });
};

const deleteProduct = (req, res) => {
  const { id } = req.params;
  // Lógica para remover o produto com base no ID da requisição
  // da lista de produtos mockados
  // ...
  res.json({ success: true });
};

module.exports = {
  getProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
};
