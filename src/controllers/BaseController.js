const BaseController = app => {
  const handleRequest = (params, validate, action, errorMessage = 'Erro ao processar a requisição') => {
    if (!params || typeof params !== 'object' || Array.isArray(params)) {
      throw new Error(`${errorMessage}: No parameters provided`);
    }

    if (!validate || typeof validate !== 'function') {
      throw new Error(`${errorMessage}: função de validação não fornecida ou inválida`);
    }

    const validationResult = validate(params);

    if (validationResult?.error) {
      throw new Error(`${errorMessage}: ${validationResult.error}`);
    }

    try {
      return action(params);
    } catch (error) {
      throw new Error(`${errorMessage}: ${error.message}`);
    }
  };

  const createController = rules => {
    const controller = {};

    rules.forEach(rule => {
      const { name, validate, action, errorMessage } = rule;

      if (!name) {
        throw new Error('A rule must have a name');
      }

      if (!validate || typeof validate !== 'function') {
        throw new Error('A rule must have a validation function');
      }

      if (!action || typeof action !== 'function') {
        throw new Error('A rule must have an action function');
      }

      controller[name] = params => {
        try {
          return handleRequest(params, validate, action, errorMessage);
        } catch (error) {
          // Lidar com o erro
          app.logger.error(`Erro ao executar o método ${name}: ${error.message}`);
          throw error;
        }
      };
    });

    return controller;
  };

  const getList = (req, res) => {
    const params = {}; // Parâmetros da requisição

    const validate = () => {
      // Lógica de validação personalizada
      // Retornar { error: 'mensagem de erro' } caso a validação falhe
      return { error: null };
    };

    const action = () => {
      // Lógica de obtenção da lista
      return app.mockUsers;
    };

    // Chamar a função genérica para lidar com a requisição
    try {
      const result = handleRequest(params, validate, action);
      res.json(result);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  return {
    handleRequest,
    createController,
    getList,
  };
};

module.exports = BaseController;
