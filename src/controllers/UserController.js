// UserController.js

const mockUsers = [
  { id: 1, name: 'John Doe', email: 'john@example.com', password: '123456' },
  { id: 2, name: 'Jane Smith', email: 'jane@example.com', password: 'abcdef' },
  // Outros usuários mockados...
];

// Método para retornar a lista de usuários
const getUsers = (req, res) => {
  res.json(mockUsers);
};

// Método para buscar um usuário por ‘ID’
const getUserById = (req, res) => {
  const { id } = req.params;
  const user = mockUsers.find(user => user.id === parseInt(id));
  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ error: 'Usuário não encontrado' });
  }
};

// Método para buscar um usuário por 'email'
const getUserByEmail = (req, res) => {
  const { email } = req.params;
  const user = mockUsers.find(user => user.email === email);
  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ error: 'Usuário não encontrado' });
  }
};

// Método para criar um usuário
const createUser = (req, res) => {
  // Lógica para criar um novo usuário com base nos dados da requisição
  // e adicionar o usuário à lista de usuários mockados
  // ...
  res.status(201).json({ success: true });
};

// Método para atualizar um usuário por ‘ID’
const updateUser = (req, res) => {
  const { id } = req.params;
  // Lógica para atualizar os dados do usuário com base nos dados da requisição
  // e atualizar o usuário na lista de usuários mockados
  // ...
  res.json({ success: true });
};

// Método para remover um usuário por ‘ID’
const deleteUser = (req, res) => {
  const { id } = req.params;
  // Lógica para remover o usuário com base no ID da requisição
  // da lista de usuários mockados
  // ...
  res.json({ success: true });
};

// Exporte os métodos do controlador para serem utilizados nas rotas
module.exports = {
  getUsers,
  getUserById,
  getUserByEmail,
  createUser,
  updateUser,
  deleteUser,
};
