const Axios = app => {
  return {
    baseURL: 'http://localhost:3002',
    timeout: 5000, // Tempo limite de 5 segundos
  };
};

module.exports = Axios;
