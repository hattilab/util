const Cors = app => {
  return {
    origin: ['http://localhost:8080', 'http://127.0.0.1:5173'], // Defina a origem permitida para as requisições
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
    maxAge: 86400,
  };
};

module.exports = Cors;
