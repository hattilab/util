const Express = app => {
  return {
    supportedMethods: ['get', 'post', 'put', 'delete'],
  };
};

module.exports = Express;
