# Util

API de Utilidades (Util Service) - Lista TODO
- [ ] Definir as funcionalidades comuns e utilitários a serem incluídos na API de Utilidades.
  - [ ] Validação de Dados
    - [ ] Validar a entrada de dados, como verificação de campos obrigatórios, formato de e-mail, comprimento de senha, etc.
    - [ ] Verificar a integridade dos dados antes de processá-los.
    - [ ] Formatação de Dados
  - [ ] Formatar dados para exibição ou armazenamento, como formatar datas, números, moedas, etc.
    - [ ] Normalizar dados para garantir consistência, como transformar strings em maiúsculas ou minúsculas.
    - [ ] Manipulação de Datas e Horas
  - [ ] Manipular datas e horários, como cálculos de diferença entre datas, adição ou subtração de dias, formatação de datas, etc.
    - [ ] Converter datas e horários em diferentes fusos horários.
    - [ ] Criptografia e Hash de Senhas
  - [ ] Criptografar senhas para armazenamento seguro no banco de dados.
    - [ ] Verificar a validade de senhas ao fazer comparações de hash.
    - [ ] Geração de Tokens e Chaves
  - [ ] Gerar tokens de acesso, como tokens JWT (JSON Web Tokens), para autenticação e autorização.
    - [ ] Gerar chaves de API para autenticação de aplicativos clientes.
    - [ ] Manipulação de Strings
  - [ ] Manipular strings, como remover espaços em branco, substituir caracteres especiais, dividir ou juntar strings, etc.
    - [ ] Fornecer funções para codificação e decodificação de strings, como base64.
    - [ ] Validação de Formulários
  - [ ] Validar formulários comuns, como verificação de endereço de e-mail, número de telefone, CPF, CNPJ, etc.
    - [ ] Tratamento de Erros e Exceções
  - [ ] Fornecer um mecanismo de tratamento de erros e exceções padronizado para todas as APIs do projeto.
    - [ ] Gerar mensagens de erro claras e consistentes para facilitar a depuração.
- [ ] Projetar a estrutura da API de Utilidades, incluindo as rotas e os endpoints necessários para cada funcionalidade.
    /util
    ├── /src
    │   ├── /validation
    │   │   ├── validationUtil.js
    │   │   └── ...
    │   ├── /formatting
    │   │   ├── formattingUtil.js
    │   │   └── ...
    │   ├── /datetime
    │   │   ├── datetimeUtil.js
    │   │   └── ...
    │   ├── /encryption
    │   │   ├── encryptionUtil.js
    │   │   └── ...
    │   ├── /token
    │   │   ├── tokenUtil.js
    │   │   └── ...
    │   ├── /string
    │   │   ├── stringUtil.js
    │   │   └── ...
    │   ├── /form
    │   │   ├── formUtil.js
    │   │   └── ...
    │   └── /error
    │       ├── errorUtil.js
    │       └── ...
    ├── package.json
    ├── index.js
    └── README.md
    
    validationUtil.js: Contém funções relacionadas à validação de dados.
    formattingUtil.js: Contém funções relacionadas à formatação de dados.
    datetimeUtil.js: Contém funções relacionadas à manipulação de datas e horas.
    encryptionUtil.js: Contém funções relacionadas à criptografia e hash de senhas.
    tokenUtil.js: Contém funções relacionadas à geração e manipulação de tokens.
    stringUtil.js: Contém funções relacionadas à manipulação de strings.
    formUtil.js: Contém funções relacionadas à validação de formulários.
    errorUtil.js: Contém funções relacionadas ao tratamento de erros e exceções.
    O arquivo index.js pode ser usado para exportar os utilitários como um módulo que pode ser importado e utilizado em outros projetos.
    O arquivo README.md pode conter a documentação do repositório Util, explicando a finalidade de cada utilitário, como usá-los e quais são suas dependências.

- [ ] Implementar as funcionalidades da API de Utilidades:
- [ ] Escrever testes unitários para cada funcionalidade implementada.
- [ ] Documentar a API de Utilidades, incluindo a descrição das funcionalidades, os endpoints disponíveis, os parâmetros esperados e os exemplos de uso.
- [ ] Realizar testes e ajustes, verificando se as funcionalidades estão funcionando conforme o esperado.
- [ ] Integrar a API de Utilidades com outras APIs do projeto, aproveitando as funcionalidades comuns e utilitários disponibilizados.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hattilab/util.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hattilab/util/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
