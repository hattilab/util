module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  plugins: ['prettier'],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    'prettier/prettier': 'error',
    'array-callback-return': 'off',
    'no-useless-escape': 'off',
    'no-plusplus': 'off',
    'no-underscore-dangle': 'off',
    'no-loop-func': 'off',
    'no-await-in-loop': 'off',
    'no-nested-ternary': 'off',
    camelcase: 'off',
    'no-case-declarations': 'off',
    'no-param-reassign': 'off',
    radix: 'off',
    'no-shadow': 'off',
    'no-restricted-syntax': 'off',
  },
};
